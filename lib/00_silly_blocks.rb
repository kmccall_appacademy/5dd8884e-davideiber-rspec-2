def reverser
  yield.split.map do |word|
    word.split("").reverse.join
  end.join(" ")
end

def adder(add=1)
  num = yield
	num = num + add
end

def repeater(num=1)
	num.times do
		yield
	end
end
