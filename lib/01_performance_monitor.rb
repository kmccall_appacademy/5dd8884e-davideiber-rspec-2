def measure(num=1)
  time_now = Time.now
  num.times { yield }
  return (Time.now - time_now) / num
end
